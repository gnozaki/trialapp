Rails.application.routes.draw do

  root 'products#index'

  resources :products
  post 'products/process' => 'products#import_via_url'

end
